<p align="center">
 <img src ="https://gitlab.com/MaginStudios/time/raw/master/time_app.png", height=350/>
</p>

# Time · Android App
This is a time tracking application. It includes the following operations:
- Add a new entry
- Edit an entry
- Delete an entry
- Get a list of entries

Technical details:
- **Java** · Language
- **MVVM** · Architecture
- **ViewModel, LiveData** · Architecture Components
- **Room** · Persistence