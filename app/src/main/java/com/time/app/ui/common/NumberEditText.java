package com.time.app.ui.common;

import android.content.Context;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.View;

public class NumberEditText extends android.support.v7.widget.AppCompatEditText {

	public NumberEditText(Context context) {
		super(context);
		init();
	}

	public NumberEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public NumberEditText(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	private void init() {
		setOnFocusChangeListener(new DefaultFocusChangeListener());
		setOnEditorActionListener(new DefaultEditorActionListener());
		setTransformationMethod(new NumericKeyBoardTransformationMethod());
	}

	private class NumericKeyBoardTransformationMethod extends PasswordTransformationMethod {
		@Override
		public CharSequence getTransformation(CharSequence source, View view) {
			return source;
		}
	}
}
