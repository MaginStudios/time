package com.time.app.ui.common;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class DefaultFocusChangeListener implements View.OnFocusChangeListener {
	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		InputMethodManager inputMethodManager = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
	}
}
