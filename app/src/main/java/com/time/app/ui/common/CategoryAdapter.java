package com.time.app.ui.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.time.app.R;

import java.util.List;

public class CategoryAdapter extends ArrayAdapter<Category> {
	private Context context;
	private List<Category> list;

	public CategoryAdapter(@NonNull Context context, int resource, @NonNull List<Category> list) {
		super(context, resource, list);
		this.context = context;
		this.list = list;
	}

	@Override
	public long getItemId(int position) {
		return list.get(position).id;
	}

	@Override
	public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		return getView(position, convertView, parent);
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		ViewHolder holder;
		Category currentCategory = list.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.category_view, parent, false);
			holder.icon = convertView.findViewById(R.id.icon);
			holder.description = convertView.findViewById(R.id.description);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.icon.setImageResource(currentCategory.icon);
		holder.icon.setColorFilter(ContextCompat.getColor(context, currentCategory.color));
		holder.description.setText(currentCategory.description);
		return convertView;
	}

	class ViewHolder {
		TextView description;
		ImageView icon;
	}
}
