package com.time.app.ui.time_list;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.time.app.db.TimeDatabase;
import com.time.app.db.TimeModel;

import java.util.List;

public class TimeListViewModel extends AndroidViewModel {

	private final LiveData<List<TimeModel>> timeList;

	private TimeDatabase timeDatabase;

	public TimeListViewModel(Application application) {
		super(application);

		timeDatabase = TimeDatabase.getDatabase(this.getApplication());

		timeList = timeDatabase.timeModel().getAllTimeList();
	}

	public LiveData<List<TimeModel>> getTimeList() {
		return timeList;
	}
}