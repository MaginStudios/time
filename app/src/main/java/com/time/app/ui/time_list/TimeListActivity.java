package com.time.app.ui.time_list;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.time.app.R;
import com.time.app.db.TimeModel;
import com.time.app.ui.time_detail.TimeDetailActivity;
import com.time.app.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;

public class TimeListActivity extends AppCompatActivity implements View.OnClickListener {

	private TimeListRecyclerViewAdapter recyclerViewAdapter;
	private TextView totalTextView;
	private TextView emptyStateTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_time_list);
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle(getString(R.string.timeListTitle));
		}

		FloatingActionButton fab = findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				startActivity(new Intent(TimeListActivity.this, TimeDetailActivity.class));
			}
		});

		RecyclerView recyclerView = findViewById(R.id.recyclerView);
		recyclerViewAdapter = new TimeListRecyclerViewAdapter(new ArrayList<TimeModel>(), this);
		recyclerView.setLayoutManager(new LinearLayoutManager(this));

		recyclerView.setAdapter(recyclerViewAdapter);

		totalTextView = findViewById(R.id.totalTextView);
		emptyStateTextView = findViewById(R.id.emptyStateTextView);

		TimeListViewModel viewModel = ViewModelProviders.of(this).get(TimeListViewModel.class);
		viewModel.getTimeList().observe(TimeListActivity.this, new Observer<List<TimeModel>>() {
			@Override
			public void onChanged(@Nullable List<TimeModel> itemAndPeople) {
				recyclerViewAdapter.addItems(itemAndPeople);
				updateEmptyState();
				totalTextView.setText(TimeUtils.getDurationStringFromLong(recyclerViewAdapter.getTotalDuration()));
			}
		});
	}

	private void updateEmptyState() {
		emptyStateTextView.setVisibility(recyclerViewAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(TimeListActivity.this, TimeDetailActivity.class);
		intent.putExtra("timeModel", (TimeModel)v.getTag());
		startActivity(intent);
	}
}