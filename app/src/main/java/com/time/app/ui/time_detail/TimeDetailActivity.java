package com.time.app.ui.time_detail;

import android.app.TimePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.time.app.R;
import com.time.app.db.TimeModel;
import com.time.app.ui.common.Category;
import com.time.app.ui.common.CategoryAdapter;
import com.time.app.ui.common.DefaultEditorActionListener;
import com.time.app.ui.common.DefaultFocusChangeListener;
import com.time.app.ui.common.NumberEditText;
import com.time.app.utils.TimeUtils;
import com.time.app.utils.DialogUtils;

import java.util.Arrays;
import java.util.Calendar;

public class TimeDetailActivity extends AppCompatActivity {

	private Calendar calendar;
	private EditText descriptionEditText;
	private NumberEditText durationHoursEditText;
	private NumberEditText durationMinutesEditText;
	private NumberEditText durationSecondsEditText;
	private TextView dateTextView;
	private TextView timeTextView;
	private AppCompatButton startTimerButton;
	private Spinner categoriesSpinner;
	private TimeDetailViewModel timeDetailViewModel;
	private TimeModel timeModel;
	private boolean isEditMode;
	private long duration = 0;
	private CountDownTimer countDownTimer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_time_detail);
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		timeModel = (TimeModel) getIntent().getSerializableExtra("timeModel");
		isEditMode = timeModel != null;
		calendar = Calendar.getInstance();

		descriptionEditText = findViewById(R.id.descriptionEditText);
		durationHoursEditText = findViewById(R.id.durationHoursEditText);
		durationMinutesEditText = findViewById(R.id.durationMinutesEditText);
		durationSecondsEditText = findViewById(R.id.durationSecondsEditText);
		dateTextView = findViewById(R.id.dateTextView);
		timeTextView = findViewById(R.id.timeTextView);
		timeDetailViewModel = ViewModelProviders.of(this).get(TimeDetailViewModel.class);
		startTimerButton = findViewById(R.id.startTimer);

		categoriesSpinner = findViewById(R.id.categoriesSpinner);
		CategoryAdapter adapter = new CategoryAdapter(this, R.layout.category_view, Arrays.asList(Category.values()));
		adapter.setDropDownViewResource(R.layout.category_view);
		categoriesSpinner.setAdapter(adapter);
		descriptionEditText.setHint(categoriesSpinner.getSelectedItem().toString());
		categoriesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				descriptionEditText.setHint(categoriesSpinner.getSelectedItem().toString());
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		if (isEditMode) {
			duration = timeModel.getDuration();
			descriptionEditText.setText(timeModel.getDescription());
			updateDurationString(timeModel.getDuration());
			calendar.setTimeInMillis(timeModel.getTimestamp().getTime());
			for (int i = 0; i < categoriesSpinner.getCount(); i++) {
				long itemIdAtPosition = categoriesSpinner.getItemIdAtPosition(i);
				if (itemIdAtPosition == timeModel.getCategory()) {
					categoriesSpinner.setSelection(i);
					break;
				}
			}
		}

		descriptionEditText.setOnFocusChangeListener(new DefaultFocusChangeListener());
		descriptionEditText.setOnEditorActionListener(new DefaultEditorActionListener());

		timeTextView.setText(TimeUtils.formatTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE)));
		dateTextView.setText(TimeUtils.formatDate(calendar.getTimeInMillis()));

		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle(getString(isEditMode ? R.string.timeDetailEditTitle : R.string.timeDetailAddTitle));
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}

		FloatingActionButton fab = findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (isEditMode) {
					timeDetailViewModel.editTime(new TimeModel(
							timeModel.id,
							getDescription(),
							TimeUtils.getDurationLongFromStrings(
									durationHoursEditText.getText().toString(),
									durationMinutesEditText.getText().toString(),
									durationSecondsEditText.getText().toString()
							),
							calendar.getTime(),
							(int)categoriesSpinner.getSelectedItemId()));
					finish();
					Toast.makeText(TimeDetailActivity.this, getString(R.string.timeUpdated), Toast.LENGTH_SHORT).show();
				} else {
					timeDetailViewModel.addTime(new TimeModel(
							getDescription(),
							TimeUtils.getDurationLongFromStrings(
									durationHoursEditText.getText().toString(),
									durationMinutesEditText.getText().toString(),
									durationSecondsEditText.getText().toString()
							),
							calendar.getTime(),
							(int)categoriesSpinner.getSelectedItemId()
					));
					finish();
					Toast.makeText(TimeDetailActivity.this, getString(R.string.timeAdded), Toast.LENGTH_SHORT).show();
				}
			}
		});

		findViewById(R.id.timeContainer).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogUtils.showTimePickerDialog(TimeDetailActivity.this, calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), new TimePickerDialog.OnTimeSetListener() {
					@Override
					public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
						calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
						calendar.set(Calendar.MINUTE, minute);
						timeTextView.setText(TimeUtils.formatTime(hourOfDay, minute));
					}
				});
			}
		});

		findViewById(R.id.dateContainer).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogUtils.showDatePickerDialog(TimeDetailActivity.this, Calendar.getInstance(), new DialogUtils.DatePickerListener() {
					@Override
					public void onDateSelected(Calendar dateSelected) {
						calendar.set(Calendar.YEAR, dateSelected.get(Calendar.YEAR));
						calendar.set(Calendar.MONTH, dateSelected.get(Calendar.MONTH));
						calendar.set(Calendar.DAY_OF_MONTH, dateSelected.get(Calendar.DAY_OF_MONTH));
						dateTextView.setText(TimeUtils.formatDate(dateSelected.getTimeInMillis()));
					}
				});
			}
		});

		startTimerButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (v.getTag() == null) {
					v.setTag("counting");
					countDownTimer = new CountDownTimer(Long.MAX_VALUE, 1000) {
						public void onTick(long millisUntilFinished) {
							updateDurationString(duration++);
						}

						public void onFinish() {
						}

					}.start();
					startTimerButton.setText(getString(R.string.stopTimer));
					startTimerButton.getBackground().setColorFilter(ContextCompat.getColor(TimeDetailActivity.this, R.color.red), PorterDuff.Mode.SRC_ATOP);
				} else {
					v.setTag(null);
					countDownTimer.cancel();
					startTimerButton.setText(getString(R.string.startTimer));
					startTimerButton.getBackground().setColorFilter(ContextCompat.getColor(TimeDetailActivity.this, R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
				}
			}
		});

		findViewById(R.id.resetTimer).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				updateDurationString(duration = 0);
			}
		});
	}

	private String getDescription() {
		String description = descriptionEditText.getText().toString();
		return description.isEmpty() ? categoriesSpinner.getSelectedItem().toString() : description;
	}

	private void updateDurationString(long duration) {
		String[] durationParts = TimeUtils.getDurationStringFromLong(duration).split(":");
		durationHoursEditText.setText(durationParts[0]);
		durationMinutesEditText.setText(durationParts[1]);
		durationSecondsEditText.setText(durationParts[2]);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (isEditMode) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.edit_time_menu, menu);
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_delete) {
			DialogUtils.showConfirmationDialog(this, getString(R.string.deleteTime), getString(R.string.deleteTimeMessage), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					timeDetailViewModel.deleteTime(timeModel);
					finish();
					Toast.makeText(TimeDetailActivity.this, getString(R.string.timeDeleted), Toast.LENGTH_SHORT).show();
				}
			});
		} else if (item.getItemId() == android.R.id.home) {
			onBackPressed();
		}
		return true;
	}
}