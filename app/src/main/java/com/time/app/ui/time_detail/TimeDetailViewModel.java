package com.time.app.ui.time_detail;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.os.AsyncTask;
import com.time.app.db.TimeDatabase;
import com.time.app.db.TimeModel;


public class TimeDetailViewModel extends AndroidViewModel {

	private TimeDatabase timeDatabase;

	public TimeDetailViewModel(Application application) {
		super(application);
		timeDatabase = TimeDatabase.getDatabase(this.getApplication());
	}

	void addTime(final TimeModel timeModel) {
		new AddAsyncTask(timeDatabase).execute(timeModel);
	}

	void editTime(final TimeModel timeModel) {
		new EditAsyncTask(timeDatabase).execute(timeModel);
	}

	void deleteTime(final TimeModel timeModel) {
		new DeleteAsyncTask(timeDatabase).execute(timeModel);
	}

	private static class AddAsyncTask extends AsyncTask<TimeModel, Void, Void> {
		private TimeDatabase db;

		AddAsyncTask(TimeDatabase appDatabase) {
			db = appDatabase;
		}

		@Override
		protected Void doInBackground(final TimeModel... params) {
			db.timeModel().addTime(params[0]);
			return null;
		}
	}

	private static class EditAsyncTask extends AsyncTask<TimeModel, Void, Void> {
		private TimeDatabase db;

		EditAsyncTask(TimeDatabase appDatabase) {
			db = appDatabase;
		}

		@Override
		protected Void doInBackground(final TimeModel... params) {
			db.timeModel().editTime(params[0]);
			return null;
		}
	}

	private static class DeleteAsyncTask extends AsyncTask<TimeModel, Void, Void> {
		private TimeDatabase db;

		DeleteAsyncTask(TimeDatabase appDatabase) {
			db = appDatabase;
		}

		@Override
		protected Void doInBackground(final TimeModel... params) {
			db.timeModel().deleteTime(params[0]);
			return null;
		}
	}
}