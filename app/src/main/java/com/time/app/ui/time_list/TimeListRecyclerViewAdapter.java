package com.time.app.ui.time_list;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.time.app.R;
import com.time.app.db.TimeModel;
import com.time.app.ui.common.Category;
import com.time.app.utils.TimeUtils;

import java.util.List;

public class TimeListRecyclerViewAdapter extends RecyclerView.Adapter<TimeListRecyclerViewAdapter.TimeListRecyclerViewHolder> {

	private List<TimeModel> timeModelList;
	private View.OnClickListener onClickListener;

	TimeListRecyclerViewAdapter(List<TimeModel> timeModelList, View.OnClickListener onClickListener) {
		this.timeModelList = timeModelList;
		this.onClickListener = onClickListener;
	}

	@NonNull
	@Override
	public TimeListRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new TimeListRecyclerViewHolder(LayoutInflater.from(parent.getContext())
				.inflate(R.layout.time_list_item, parent, false));
	}

	@Override
	public void onBindViewHolder(final TimeListRecyclerViewHolder holder, int position) {
		TimeModel timeModel = timeModelList.get(position);
		holder.descriptionTextView.setText(timeModel.getDescription());
		holder.durationTextView.setText(TimeUtils.getDurationStringFromLong(timeModel.getDuration()));
		holder.dateTextView.setText(TimeUtils.formatDateTime(timeModel.getTimestamp().getTime()));
		holder.categoryIcon.setImageResource(Category.findById(timeModel.getCategory()).icon);
		holder.categoryIcon.setColorFilter(ContextCompat.getColor(holder.categoryIcon.getContext(), Category.findById(timeModel.getCategory()).color));
		holder.itemView.setTag(timeModel);
		holder.itemView.setOnClickListener(onClickListener);
	}

	@Override
	public int getItemCount() {
		return timeModelList.size();
	}

	void addItems(List<TimeModel> timeModelList) {
		this.timeModelList = timeModelList;
		notifyDataSetChanged();
	}

	long getTotalDuration() {
		long total = 0;
		for (TimeModel timeModel : timeModelList) {
			total += timeModel.getDuration();
		}
		return total;
	}

	static class TimeListRecyclerViewHolder extends RecyclerView.ViewHolder {
		private TextView descriptionTextView;
		private TextView durationTextView;
		private TextView dateTextView;
		private ImageView categoryIcon;

		TimeListRecyclerViewHolder(View view) {
			super(view);
			descriptionTextView = view.findViewById(R.id.descriptionTextView);
			durationTextView = view.findViewById(R.id.durationTextView);
			dateTextView = view.findViewById(R.id.dateTextView);
			categoryIcon = view.findViewById(R.id.categoryIcon);
		}
	}
}