package com.time.app.ui.common;

import android.support.annotation.NonNull;

import com.time.app.R;

public enum Category {
	WORK(1, "Work",R.drawable.ic_briefcase, R.color.catWork),
	STUDY(2, "Study", R.drawable.ic_school, R.color.catStudy),
	BUSINESS(3, "Business", R.drawable.ic_cash_multiple, R.color.catBusiness),
	RELATIONSHIPS(4,"Relationships", R.drawable.ic_account_multiple, R.color.catRelationships),
	ENTERTAINMENT(5, "Entertainment", R.drawable.ic_human_handsup, R.color.catEntertainment),
	WELLNESS(6, "Wellness", R.drawable.ic_run, R.color.catWellness),
	HOME(7, "Home", R.drawable.ic_home, R.color.catHome),
	SOCIAL_MEDIA(8, "Social media", R.drawable.ic_heart, R.color.catSocialMedia),
	OTHERS(9, "Others", R.drawable.ic_receipt, R.color.catOthers);

	public int id;
	public String description;
	public int icon;
	public int color;

	Category(int id, String description, int icon, int color) {
		this.id = id;
		this.description = description;
		this.icon = icon;
		this.color = color;
	}

	public static Category findById(int id) {
		for (Category category : values()) {
			if (category.id == id) {
				return category;
			}
		}
		return WORK;
	}

	@NonNull
	@Override
	public String toString() {
		return description;
	}
}