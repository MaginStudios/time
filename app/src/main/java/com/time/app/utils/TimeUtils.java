package com.time.app.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeUtils {
	public static String formatDateTime(long timestamp) {
		Calendar date = Calendar.getInstance();
		date.setTimeInMillis(timestamp);
		String format = "HH:mm · dd/MM/yy";
		Calendar tempDate = Calendar.getInstance();
		if (date.get(Calendar.DATE) == tempDate.get(Calendar.DATE)) {
			format = "HH:mm · 'Today'";
		} else {
			tempDate.add(Calendar.DATE, -1);
			if (date.get(Calendar.DATE) == tempDate.get(Calendar.DATE)) {
				format = "HH:mm · 'Yesterday'";
			}
		}
		return new SimpleDateFormat(format, Locale.US).format(new Date(date.getTimeInMillis()));
	}

	public static String formatDate(long timestamp) {
		Calendar date = Calendar.getInstance();
		date.setTimeInMillis(timestamp);
		Calendar tempDate = Calendar.getInstance();
		if (date.get(Calendar.DATE) == tempDate.get(Calendar.DATE)) {
			return "Today";
		} else {
			tempDate.add(Calendar.DATE, -1);
			if (date.get(Calendar.DATE) == tempDate.get(Calendar.DATE)) {
				return "Yesterday";
			}
		}
		return new SimpleDateFormat("dd/MM/yy", Locale.US).format(new Date(date.getTimeInMillis()));
	}

	public static String formatTime(int hour, int minute) {
		return String.format(Locale.US,"%02d", hour) + ":" + String.format(Locale.US, "%02d", minute);
	}

	public static String getDurationStringFromLong(long duration) {
		return String.format(Locale.US, "%02d", duration/60/60) + ":" + String.format(Locale.US, "%02d", (duration/60)%60) + ":" + String.format(Locale.US, "%02d", (duration%60)%60);
	}

	public static long getDurationLongFromStrings(String hours, String minutes, String seconds) {
		return (Integer.parseInt(hours.isEmpty() ? "0" : hours) * 60 * 60) +
				(Integer.parseInt(minutes.isEmpty() ? "0" : minutes) * 60) +
				(Integer.parseInt(seconds.isEmpty() ? "0" : seconds));
	}
}
