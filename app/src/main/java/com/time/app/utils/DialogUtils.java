package com.time.app.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.format.DateFormat;
import android.widget.DatePicker;

import com.time.app.R;

import java.util.Calendar;


public class DialogUtils {
	public static AlertDialog showConfirmationDialog(Activity context, String title, String message, DialogInterface.OnClickListener listener) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getString(R.string.accept), listener);
		alertDialog.show();
		return alertDialog;
	}

	public static TimePickerDialog showTimePickerDialog(Context context, int hour, int minute, TimePickerDialog.OnTimeSetListener listener) {
		TimePickerDialog timePickerDialog = new TimePickerDialog(context, listener, hour, minute, DateFormat.is24HourFormat(context));
		timePickerDialog.show();
		return timePickerDialog;
	}

	public interface DatePickerListener {
		void onDateSelected(Calendar dateSelected);
	}

	public static DatePickerDialog showDatePickerDialog(Activity activity, Calendar defaultDate, final DatePickerListener listener) {
		DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
				Calendar newDateFrom = Calendar.getInstance();
				newDateFrom.set(year, month, dayOfMonth);
				listener.onDateSelected(newDateFrom);
			}
		};
		DatePickerDialog datePickerDialog = new DatePickerDialog(activity, pickerListener, defaultDate.get(Calendar.YEAR), defaultDate.get(Calendar.MONTH), defaultDate.get(Calendar.DAY_OF_MONTH));
		datePickerDialog.show();
		return datePickerDialog;
	}
}
