package com.time.app.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {TimeModel.class}, version = 1, exportSchema = false)
public abstract class TimeDatabase extends RoomDatabase {

	private static TimeDatabase INSTANCE;

	public static TimeDatabase getDatabase(Context context) {
		if (INSTANCE == null) {
			INSTANCE =
					Room.databaseBuilder(context.getApplicationContext(), TimeDatabase.class, "timedb")
							.build();
		}
		return INSTANCE;
	}

	public abstract TimeModelDao timeModel();
}