package com.time.app.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
@TypeConverters(DateConverter.class)
public interface TimeModelDao {

	@Query("SELECT * FROM TimeModel ORDER BY timestamp DESC")
	LiveData<List<TimeModel>> getAllTimeList();

	@Query("SELECT * FROM TimeModel WHERE id = :id")
	TimeModel getItemById(String id);

	@Insert(onConflict = REPLACE)
	void addTime(TimeModel timeModel);

	@Delete
	void deleteTime(TimeModel timeModel);

	@Update
	void editTime(TimeModel timeModel);
}