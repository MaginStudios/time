package com.time.app.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.io.Serializable;
import java.util.Date;
@Entity
public class TimeModel implements Serializable {

	@PrimaryKey(autoGenerate = true)
	public int id;
	private String description;
	private long duration;
	@TypeConverters(DateConverter.class)
	private Date timestamp;
	private int category;

	public TimeModel(int id, String description, long duration, Date timestamp, int category) {
		this.id = id;
		this.description = description;
		this.duration = duration;
		this.timestamp = timestamp;
		this.category = category;
	}

	@Ignore
	public TimeModel(String description, long duration, Date timestamp, int category) {
		this.description = description;
		this.duration = duration;
		this.timestamp = timestamp;
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public long getDuration() {
		return duration;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public int getCategory() {
		return category;
	}
}